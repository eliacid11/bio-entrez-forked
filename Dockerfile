FROM ubuntu:latest

# Instalar Node.js y npm
RUN apt-get update \
    && apt-get install -y nodejs npm \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY . /app
CMD ["npm", "start"]

