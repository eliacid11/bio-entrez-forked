#!/bin/bash

# Instalar Docker si no está instalado
sudo apt update
sudo apt install -y docker.io

# Construir la imagen de Docker
docker build -t xtec/bio-entrez .

